import time
import pytest

from pages.LoginDemoPage import LoginDemoPage
from testdata.LoginData import LoginData
from utilities.BaseClass import BaseClass


class TestOne(BaseClass):
    def test_login_demo(self, setup, getData):
        log = self.getLogger()
        loginpage = LoginDemoPage(self.driver)

        log.info("loginpage.getuserPassword")

        loginpage.getuserID().send_keys(getData["userid"])
        loginpage.getuserPassword().send_keys(getData["password"])
        loginpage.getforgotpasswd().click()
        time.sleep(5)
        loginpage.getcancel().click()
        time.sleep(5)
        log.debug("loginpage.getcancel")
        loginform = loginpage.getloginform().text
        passwordfield = loginpage.getloginpassfield().text

        log.debug("loginpage.getloginpassfield()")
        print(passwordfield)
        assert ("Password" in loginform)
        assert ("Use Custom Domain" in passwordfield)

    @pytest.fixture(params=LoginData.test_login_data)
    def getData(self, request):
        return request.param

    # @pytest.fixture(params=LoginData.getTestData("Testcase1"))
    # def getData(self, request):
    #     return request
    # #
